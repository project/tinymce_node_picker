
Drupal.wysiwyg.plugins["nodepicker"] = {    
  /**
   * Execute the button.
   */
  invoke: function(data, settings, instanceId) {  	
  	// Options to pass to the dialog.
		var options = { id: instanceId, content: data.content };
 
    // Open dialogue.
    Drupal.wysiwyg.instances[instanceId].openDialog(settings.dialog, options);
  }
};